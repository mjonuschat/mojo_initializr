mod.SHARED {
    defaultLanguageFlag = de
    defaultLanguageLabel = LLL:EXT:mojo:initializr/Resources/Private/Language/locallang_db.xml:label.default_language
}

## Label der Überschriften anpassen
TCEFORM.tt_content.header_layout.altLabels.0 = LLL:EXT:mojo:initializr/Resources/Private/Language/locallang_ttcontent.xml:label.header_layout.0
TCEFORM.tt_content.header_layout.altLabels.1 = LLL:EXT:mojo:initializr/Resources/Private/Language/locallang_ttcontent.xml:label.header_layout.1
TCEFORM.tt_content.header_layout.altLabels.2 = LLL:EXT:mojo:initializr/Resources/Private/Language/locallang_ttcontent.xml:label.header_layout.2
TCEFORM.tt_content.header_layout.altLabels.3 = LLL:EXT:mojo:initializr/Resources/Private/Language/locallang_ttcontent.xml:label.header_layout.3
TCEFORM.tt_content.header_layout.altLabels.4 = LLL:EXT:mojo:initializr/Resources/Private/Language/locallang_ttcontent.xml:label.header_layout.4
TCEFORM.tt_content.header_layout.altLabels.5 = LLL:EXT:mojo:initializr/Resources/Private/Language/locallang_ttcontent.xml:label.header_layout.5

## Standardbreite für Bilder vorgeben (auch nochmal in der Benutzergruppe eintragen!)
TCAdefaults.tt_content.imagewidth = 150


