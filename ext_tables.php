<?php
if (!defined('TYPO3_MODE')) { die ('Access denied.'); }

# Adjustments to the be_user configuration
$TCA['be_users']['columns']['usergroup']['config']['size']                      = 10;
$TCA['be_users']['columns']['usergroup']['config']['foreign_table_where']       = ' AND be_groups.hidden=0 ORDER BY be_groups.title';
$TCA['be_users']['columns']['usergroup']['config']['maxitems']                  = 50;

# Adjustments to the fe_user configuration
$TCA['fe_users']['columns']['usergroup']['config']['size']                      = 10;
$TCA['fe_users']['columns']['usergroup']['config']['foreign_table_where']       = ' AND fe_groups.hidden=0 ORDER BY fe_groups.title';
$TCA['fe_users']['columns']['usergroup']['config']['maxitems']                  = 50;

# Adjustments to fe fe_user subgroup configuration
$TCA['fe_groups']['columns']['subgroup']['config']['size']                      = 10;
$TCA['fe_groups']['columns']['subgroup']['config']['foreign_table_where']       = ' ORDER BY fe_groups.title';

# Adjustments to tt_content access controls
$TCA['tt_content']['columns']['fe_group']['config']['maxitems']                 = 50;
$TCA['tt_content']['columns']['fe_group']['config']['size']                     = 25;
$TCA['tt_content']['columns']['fe_group']['config']['foreign_table_where']      = ' AND fe_groups.hidden=0 ORDER BY fe_groups.title';

# Adjustments to page access controls
$TCA['pages']['columns']['fe_group']['config']['maxitems']                      = 50;
$TCA['pages']['columns']['fe_group']['config']['size']                          = 25;
$TCA['pages']['columns']['fe_group']['config']['foreign_table_where']           = ' AND fe_groups.hidden=0 ORDER BY fe_groups.title';

# Adjustments to tt_news configuration and translations
if (t3lib_extMgm::isLoaded('tt_news')) {
  t3lib_div::loadTCA('tt_news');
  # Adjust palettes and fields
  $TCA['tt_news']['ctrl']['mainpalette'] = 1;
  $TCA['tt_news']['palettes']['1']['showitem'] = 'hidden,starttime,endtime,l18n_parent,sys_language_uid';
  $TCA['tt_news']['palettes']['2']['showitem'] = 'archivedate';

  $TCA['tt_news']['types']['0']['showitem'] = 'title;;1;;,type,editlock,datetime;;2;;1-1-1,author;;3;;,short,bodytext;;4;richtext:rte_transform[flag=rte_enabled|mode=ts];4-4-4,no_auto_pb,--div--;LLL:EXT:mojo_initializr/Resources/Private/Language/locallang_ttnews.xml:tt_news_categories,category,--div--;LLL:EXT:mojo_initializr/Resources/Private/Language/locallang_ttnews.xml:tt_news_files,image;;;;1-1-1,imagecaption;;5;;,news_files;;;;2-2-2,links;;;;3-3-3,related;;;;4-4-4,--div--;LLL:EXT:mojo_initializr/Resources/Private/Language/locallang_ttnews.xml:tt_news_access,fe_group;;;;;';
  $TCA['tt_news']['types']['1']['showitem'] = 'title;;1;;,type,datetime;;2;;1-1-1,author;;3;;,short,page;;4;;,--div--;LLL:EXT:mojo_initializr/Resources/Private/Language/locallang_ttnews.xml:tt_news_categories,category,--div--;LLL:EXT:mojo_initializr/Resources/Private/Language/locallang_ttnews.xml:tt_news_files,image;;;;1-1-1,imagecaption';
  $TCA['tt_news']['types']['2']['showitem'] = 'title;;1;;,type,datetime;;2;;1-1-1,author;;3;;,short,ext_url;;4;;,--div--;LLL:EXT:mojo_initializr/Resources/Private/Language/locallang_ttnews.xml:tt_news_categories,category,--div--;LLL:EXT:mojo_initializr/Resources/Private/Language/locallang_ttnews.xml:tt_news_files,image;;;;1-1-1,imagecaption';

  # Adjust translations
  $TCA['tt_news']['columns']['short']['label'] = 'LLL:EXT:mojo_initializr/Resources/Private/Language/locallang_db.xml:tt_news_subheader';
  $TCA['tt_news']['columns']['category']['label'] = 'LLL:EXT:mojo_initializr/Resources/Private/Language/locallang_db.xml:tt_news_category';
}
