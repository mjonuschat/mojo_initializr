<?php
if (!defined('TYPO3_MODE')) { die ('Access denied.'); }
$_EXTCONF = unserialize($_EXTCONF);

# Language Overrides
$GLOBALS['TYPO3_CONF_VARS']['SYS']['locallangXMLOverride']['EXT:lang/locallang_login.xml'][]  = 'EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_login.xml';
$GLOBALS['TYPO3_CONF_VARS']['SYS']['locallangXMLOverride']['EXT:tt_news/pi1/locallang.xml'][]  = 'EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_ttnews.xml';
$GLOBALS['TYPO3_CONF_VARS']['SYS']['locallangXMLOverride']['EXT:indexed_search/pi/locallang.xml'][]  = 'EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_indexedsearch.xml';

# Default settings
t3lib_extMgm::addTypoScriptConstants('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:mojo_initializr/Configuration/TypoScript/constants.txt">');
t3lib_extMgm::addTypoScriptSetup('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:mojo_initializr/Configuration/TypoScript/defaults.txt">');
t3lib_extMgm::addTypoScriptSetup('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:mojo_initializr/Configuration/TypoScript/xhtml-cleanup.txt">');
t3lib_extMgm::addTypoScriptSetup('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:mojo_initializr/Configuration/TypoScript/Plugins/mojo_sass.txtx">');
t3lib_extMgm::addTypoScriptSetup('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:mojo_initializr/Configuration/TypoScript/default_page.txt">');

# RTE + Templavoila preconfiguration
if ($_EXTCONF['setPageTSconfig']) {
  t3lib_extMgm::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:mojo_initializr/Configuration/TypoScript/pageTS/defaults.txt">');
  t3lib_extMgm::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:mojo_initializr/Configuration/TypoScript/pageTS/rte.txt">');
  t3lib_extMgm::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:mojo_initializr/Configuration/TypoScript/pageTS/templavoila.txt">');
}

# Multilanguage Configuration
if ($_EXTCONF['setMultilanguageTSconfig']) {
  t3lib_extMgm::addTypoScriptSetup('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:mojo_initializr/Configuration/TypoScript/languages.txt">');
}

# Multilanguage Configuration
if ($_EXTCONF['setPluginPreconfiguration']) {
  t3lib_extMgm::addTypoScriptSetup('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:mojo_initializr/Configuration/TypoScript/Plugins/tt_news.txt">');
  t3lib_extMgm::addTypoScriptSetup('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:mojo_initializr/Configuration/TypoScript/Plugins/indexed_search.txt">');
  t3lib_extMgm::addTypoScriptSetup('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:mojo_initializr/Configuration/TypoScript/Plugins/macina_searchbox.txt">');
  t3lib_extMgm::addTypoScriptSetup('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:mojo_initializr/Configuration/TypoScript/Plugins/perfectlightbox.txt">');
}

# Enable overrides from fileadmin folder
if ($_EXTCONF['overrideTyposcriptIncludes']) {
  t3lib_extMgm::addTypoScriptConstants('<INCLUDE_TYPOSCRIPT: source="FILE: fileadmin/templates/default/typoscripts/constants.txt">');
  t3lib_extMgm::addTypoScriptSetup('<INCLUDE_TYPOSCRIPT: source="FILE: fileadmin/templates/default/typoscripts/config.txt">');
  t3lib_extMgm::addTypoScriptSetup('<INCLUDE_TYPOSCRIPT: source="FILE: fileadmin/templates/default/typoscripts/cleanup.txt">');
  t3lib_extMgm::addTypoScriptSetup('<INCLUDE_TYPOSCRIPT: source="FILE: fileadmin/templates/default/typoscripts/language.txt">');
  t3lib_extMgm::addTypoScriptSetup('<INCLUDE_TYPOSCRIPT: source="FILE: fileadmin/templates/default/typoscripts/plugins.txt">');
  t3lib_extMgm::addTypoScriptSetup('<INCLUDE_TYPOSCRIPT: source="FILE: fileadmin/templates/default/typoscripts/library.txt">');
  t3lib_extMgm::addTypoScriptSetup('<INCLUDE_TYPOSCRIPT: source="FILE: fileadmin/templates/default/typoscripts/page.txt">');
  if ($_EXTCONF['setPageTSconfig']) {
    t3lib_extMgm::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE: fileadmin/templates/default/typoscripts/page-tsconfig.txt">');
  }
}
